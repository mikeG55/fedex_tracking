# FedEx Tracking.

Takes in a FedEx tracking number and returns a tracking summary.
You will need to add your fedex credentials to the config.py

**Requirements.**
pip, python 3.9

Install virtual env
`pip install virtualenv`

cd into api_task directory `cd api_task`

create a venv `virtualenv -p python3.9 venv`

activate the venv `source venv/bin/activate`

install all requirements `pip install -r requirements.txt`

run python `python api.py`

development server only.

http://127.0.0.1:5000/

test tracking number: **122816215025810**

expected json output
```json
{
    "carrier": "FedEx Ground",
    "status": "Delivered",
    "checkpoints": [
        {
            "description": "Delivered",
            "location": {
                "city": "Norton",
                "country": "United States",
                "postal_code": "24273",
                "state": "VA"
            },
            "time": "January 09, 2014"
        },
        {
            "description": "On FedEx vehicle for delivery",
            "location": {
                "city": "KINGSPORT",
                "country": "United States",
                "postal_code": "37663",
                "state": "TN"
            },
            "time": "January 09, 2014"
        }
    ]
}
```
