from flask import Flask, request, render_template
from forms import TrackingForm
from config import config
from fedex import FedEx
fedex = FedEx()

app = Flask(__name__)
app.config['SECRET_KEY'] = config['app']['secret_key']


@app.route("/", methods=['GET', 'POST'])
def index():
    """
    If post.
    Takes in a posted tracking number from an input form field.
    returns tracking summary

    :return: renders index html page
    """
    form = TrackingForm(request.form)
    tracking_details = None

    if request.method == 'POST':
        tracking_number = request.form['tracking_number']
        tracking_details = fedex.get_tracking_summary(tracking_number)

    if not tracking_details:
        tracking_details = ""

    return render_template('index.html', value=tracking_details, form=form)


if __name__ == '__main__':
    app.run()
