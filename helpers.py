from datetime import datetime


def format_date(date):
    """
    Reformat the date timestamp
    From: 2014-01-03T14:31:00
    To: January 03, 2014

    :param date: str
    :return: str
    """
    date_time_obj = datetime.strptime(date.rsplit("-", 1)[0], '%Y-%m-%dT%H:%M:%S')
    return date_time_obj.strftime('%B %d, %Y')
