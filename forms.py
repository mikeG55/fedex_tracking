from flask_wtf import FlaskForm
from wtforms import StringField, validators


class TrackingForm(FlaskForm):
    """
    Creates a required input field for a tracking number
    """
    tracking_number = StringField('tracking_number', validators=[validators.required()])
