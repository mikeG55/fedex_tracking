import requests
import json
import xmltodict
from config import config
from helpers import format_date


class FedEx(object):
    """
    Calls to fedex web services and processes returned output as json.
    """

    def __init__(self):
        """
        Initialise the fedex endpoint and set relevant headers

        """
        self.url = "https://wsbeta.fedex.com:443/web-services"  # test endpoint
        self.headers = {"content-type": "application/soap+xml"}

    @staticmethod
    def get_xml_data(tracking_number):
        """
        Creates the xml data to send to fedex endpoint.
        Takes in an integer tracking number and returns xml str

        :param tracking_number: int
        :return: str
        """
        return f"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
                    xmlns="http://fedex.com/ws/track/v18">
         <soapenv:Header/>
         <soapenv:Body>
            <TrackRequest>
               <WebAuthenticationDetail>
                  <ParentCredential>
                  <Key>{config['app']['parent_key']}</Key>
                  <Password>{config['app']['parent_password']}</Password> 
                  </ParentCredential>
                  <UserCredential>
                     <Key>{config['app']['user_key']}</Key>
                     <Password>{config['app']['user_password']}</Password>
                  </UserCredential>
               </WebAuthenticationDetail>
               <ClientDetail>
                  <AccountNumber>{config['app']['account_number']}</AccountNumber>
                  <MeterNumber>{config['app']['meter_number']}</MeterNumber>
               </ClientDetail>
               <TransactionDetail>
                  <CustomerTransactionId>Track_v18</CustomerTransactionId>
               </TransactionDetail>
               <Version>
                  <ServiceId>trck</ServiceId>
                  <Major>18</Major>
                  <Intermediate>0</Intermediate>
                  <Minor>0</Minor>
               </Version>
               <SelectionDetails>
                  <PackageIdentifier>
                     <Type>TRACKING_NUMBER_OR_DOORTAG</Type>
                     <Value>{tracking_number}</Value>
                  </PackageIdentifier>
               </SelectionDetails>
               <ProcessingOptions>INCLUDE_DETAILED_SCANS</ProcessingOptions>
            </TrackRequest>
         </soapenv:Body>
      </soapenv:Envelope>
        """

    def get(self, data):
        """
        Posts a request to the fedex endpoint. Uses created xml data.

        :param data: str
        :return: requests obj
        """
        return requests.post(self.url, headers=self.headers, data=data)

    @staticmethod
    def create_output(response):
        """
        Takes in a response as a dictionary.
        Parses out relevant tracking data into json.

        :param response: dict
        :return: json
        """
        envelope = response['http://schemas.xmlsoap.org/soap/envelope/:Envelope']
        body = envelope['http://schemas.xmlsoap.org/soap/envelope/:Body']
        track_reply = body['http://fedex.com/ws/track/v18:TrackReply']
        comp_tracking_details = track_reply['http://fedex.com/ws/track/v18:CompletedTrackDetails']
        tracking_details = comp_tracking_details['http://fedex.com/ws/track/v18:TrackDetails']
        notification = tracking_details['http://fedex.com/ws/track/v18:Notification']
        severity = notification['http://fedex.com/ws/track/v18:Severity']
        message = notification['http://fedex.com/ws/track/v18:Message']
        if severity == "ERROR":
            return json.dumps({'message': message}, indent=4)

        status = tracking_details['http://fedex.com/ws/track/v18:StatusDetail']

        output = {'carrier': tracking_details['http://fedex.com/ws/track/v18:OperatingCompanyOrCarrierDescription'],
                  'status': status['http://fedex.com/ws/track/v18:Description'],
                  'checkpoints': []
                  }

        events = tracking_details['http://fedex.com/ws/track/v18:Events']
        for e in events:
            address = e['http://fedex.com/ws/track/v18:Address']
            city = address['http://fedex.com/ws/track/v18:City'] if 'http://fedex.com/ws/track/v18:City' in address else "null"
            country = address['http://fedex.com/ws/track/v18:CountryName'] if 'http://fedex.com/ws/track/v18:CountryName' in address else "null"
            postal_code = address['http://fedex.com/ws/track/v18:PostalCode'] if 'http://fedex.com/ws/track/v18:PostalCode' in address else "null"
            state = address['http://fedex.com/ws/track/v18:StateOrProvinceCode'] if 'http://fedex.com/ws/track/v18:StateOrProvinceCode' in address else "null"

            event_dict = {
                'description': e['http://fedex.com/ws/track/v18:EventDescription'],
                'location': {
                    'city': city,
                    'country': country,
                    'postal_code': postal_code,
                    'state': state,
                },
                'time': format_date(e['http://fedex.com/ws/track/v18:Timestamp'])
            }
            output['checkpoints'].append(event_dict)

        return json.dumps(output, indent=4)

    def get_tracking_summary(self, tracking_number):
        """
        Takes in a tracking number.

        - Creates the xml data to send to fedex endpoint
        - Makes call to fedex endpoint
        - Turns the response content in a dict
        - Creates tracking output as json

        :param tracking_number: str
        :return: json
        """
        data = self.get_xml_data(tracking_number)
        response = self.get(data)
        data = xmltodict.parse(response.content, process_namespaces=True)
        return self.create_output(data)
